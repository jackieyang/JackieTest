//
//  main.m
//  JackieTest
//
//  Created by yangke on 16/1/17.
//  Copyright © 2016年 yangke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
